package crazy.ones.sheypoorsample

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import crazy.ones.sheypoorsample.core.LoadableData
import crazy.ones.sheypoorsample.feature.photos.Photo
import crazy.ones.sheypoorsample.feature.photos.PhotoRepository
import crazy.ones.sheypoorsample.feature.photos.presentation.list.PhotosViewModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.*
import kotlinx.coroutines.test.*
import org.junit.*
import org.junit.Assert.*
import org.junit.rules.TestRule

@ExperimentalStdlibApi
@ExperimentalCoroutinesApi
class PhotosViewModelTest {

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private val testCoroutineScope = TestCoroutineScope(testCoroutineDispatcher)

    @RelaxedMockK
    private lateinit var photoRepository: PhotoRepository

    private val photos: List<Photo> = buildList {
        repeat(5) {
            Photo(1, 1, "", "", "")
        }
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    private fun createViewModel() = PhotosViewModel(MockCoroutineDispatcher, photoRepository)

    @Test
    fun `test when try to get data, state should get updated to loading`() = testCoroutineScope.runBlockingTest {
        coEvery { photoRepository.getPhotos() } coAnswers {
            delay(300)
            photos
        }

        val viewModel = createViewModel()

        assertEquals(viewModel.photosLiveData.value, LoadableData.Loading)
    }

    @Test
    fun `test when get data successfully, state should get updated to loaded`() {
        coEvery { photoRepository.getPhotos() } returns photos

        val viewModel = createViewModel()

        assert(viewModel.photosLiveData.value is LoadableData.Loaded)
    }

    @Test
    fun `test when getting data gets failed, state should get updated to failed`() {
        coEvery { photoRepository.getPhotos() } throws Exception("error")

        val viewModel = createViewModel()

        assert(viewModel.photosLiveData.value is LoadableData.Failed)
    }

    @Test
    fun `test when state is loading, data should not be gotten again`()= testCoroutineScope.runBlockingTest {
        coEvery { photoRepository.getPhotos() } coAnswers {
            delay(300)
            photos
        }

        val viewModel = createViewModel()
        delay(100)
        viewModel.getPhotos()

        coVerify(exactly = 1) {
            photoRepository.getPhotos()
        }
    }
}