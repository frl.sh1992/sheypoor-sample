package crazy.ones.sheypoorsample

import crazy.ones.sheypoorsample.core.CoroutineDispatcherProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher

@ExperimentalCoroutinesApi
val testCoroutineDispatcher = TestCoroutineDispatcher()

@ExperimentalCoroutinesApi
object MockCoroutineDispatcher : CoroutineDispatcherProvider {

    override fun bgDispatcher(): CoroutineDispatcher = testCoroutineDispatcher

    override fun uiDispatcher(): CoroutineDispatcher = testCoroutineDispatcher

    override fun ioDispatcher(): CoroutineDispatcher = testCoroutineDispatcher
}