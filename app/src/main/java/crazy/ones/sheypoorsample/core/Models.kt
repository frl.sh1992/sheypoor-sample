package crazy.ones.sheypoorsample.core
sealed class LoadableData<out T> {
    object Loading: LoadableData<Nothing>()
    data class Loaded<T>(val data: T): LoadableData<T>()
    data class Failed<T>(val errorMessage: String?): LoadableData<T>()
}