package crazy.ones.sheypoorsample.core

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

open class BaseViewModel(private val dispatcherProvider: CoroutineDispatcherProvider) : ViewModel(),
    CoroutineScope {

    private val job = SupervisorJob()
    private val scope = CoroutineScope(dispatcherProvider.bgDispatcher() + job)

    override val coroutineContext: CoroutineContext
        get() = scope.coroutineContext

    override fun onCleared() {
        super.onCleared()
        scope.coroutineContext.cancelChildren()
    }

    internal suspend fun <T> onBg(block: suspend CoroutineScope.() -> T): T {
        return withContext(dispatcherProvider.bgDispatcher(), block)
    }

    internal suspend fun <T> onUi(block: suspend CoroutineScope.() -> T): T {
        return withContext(dispatcherProvider.uiDispatcher(), block)
    }
}