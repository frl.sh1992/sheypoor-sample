package crazy.ones.sheypoorsample.core

interface BaseUseCase<Input, Output> {
    suspend fun execute(input: Input): Output
}