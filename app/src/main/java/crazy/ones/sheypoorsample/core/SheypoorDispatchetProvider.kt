package crazy.ones.sheypoorsample.core

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

object SheypoorDispatchetProvider: CoroutineDispatcherProvider {
    override fun bgDispatcher(): CoroutineDispatcher = Dispatchers.Default

    override fun uiDispatcher(): CoroutineDispatcher = Dispatchers.Main

    override fun ioDispatcher(): CoroutineDispatcher = Dispatchers.IO
}