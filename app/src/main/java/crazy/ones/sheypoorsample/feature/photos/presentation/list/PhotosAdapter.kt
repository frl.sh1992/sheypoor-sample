package crazy.ones.sheypoorsample.feature.photos.presentation.list

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import crazy.ones.sheypoorsample.R
import crazy.ones.sheypoorsample.core.load
import crazy.ones.sheypoorsample.feature.photos.Photo
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_photo.*

class PhotosAdapter(private val onItemClicked: (Photo, sharedImage: View) -> Unit) :
    ListAdapter<Photo, PhotoViewHolder>(
        diffCallback
    ) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        return PhotoViewHolder(
            itemView,
            onItemClicked
        )
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

val diffCallback = object : DiffUtil.ItemCallback<Photo>() {
    override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean =
        oldItem == newItem
}

class PhotoViewHolder(override val containerView: View, val onItemClicked: (Photo, sharedImage: View) -> Unit) :
    RecyclerView.ViewHolder(containerView),
    LayoutContainer {
    fun bind(photo: Photo) {
        photoItemTitle.text = photo.title
        photoItemImage.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                transitionName = photo.thumbnailUrl
            }
            load(photo.url)
        }
        containerView.setOnClickListener {
            onItemClicked(photo, photoItemImage)
        }
    }
}