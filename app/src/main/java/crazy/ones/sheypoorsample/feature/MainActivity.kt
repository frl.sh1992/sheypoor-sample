package crazy.ones.sheypoorsample.feature

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import crazy.ones.sheypoorsample.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
