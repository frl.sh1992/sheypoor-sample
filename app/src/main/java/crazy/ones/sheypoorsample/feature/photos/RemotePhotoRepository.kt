package crazy.ones.sheypoorsample.feature.photos

import crazy.ones.sheypoorsample.feature.photos.Photo
import crazy.ones.sheypoorsample.feature.photos.PhotoRepository
import crazy.ones.sheypoorsample.feature.photos.PhotosApi

class RemotePhotoRepository(private val api: PhotosApi) :
    PhotoRepository {
    override suspend fun getPhotos(): List<Photo> = api.getPhotos()
}