package crazy.ones.sheypoorsample.feature.photos

import crazy.ones.sheypoorsample.core.CoroutineDispatcherProvider
import crazy.ones.sheypoorsample.core.SheypoorDispatchetProvider
import crazy.ones.sheypoorsample.feature.photos.presentation.list.GetPhotosUseCase
import crazy.ones.sheypoorsample.feature.photos.presentation.list.PhotosViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

internal val photosModule = module {
    factory<PhotosApi> {
        providePhotosApi(get())
    }

    factory<PhotoRepository> {
        RemotePhotoRepository(get())
    }

    factory {
        GetPhotosUseCase(get(), get())
    }

    single<CoroutineDispatcherProvider> {
        SheypoorDispatchetProvider
    }

    viewModel<PhotosViewModel> {
        PhotosViewModel(
            get(),
            get()
        )
    }
}
private fun providePhotosApi(retrofit: Retrofit) = retrofit.create(PhotosApi::class.java)