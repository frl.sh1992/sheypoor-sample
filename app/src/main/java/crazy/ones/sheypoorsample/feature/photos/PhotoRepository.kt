package crazy.ones.sheypoorsample.feature.photos

import crazy.ones.sheypoorsample.feature.photos.Photo

interface PhotoRepository {
    suspend fun getPhotos(): List<Photo>
}