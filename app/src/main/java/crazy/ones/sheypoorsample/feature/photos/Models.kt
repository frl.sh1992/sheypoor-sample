package crazy.ones.sheypoorsample.feature.photos

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Photo(
    @SerializedName("id")
    @ColumnInfo(name = "id")
    @PrimaryKey
    val id: Long,
    @SerializedName("albumId")
    @ColumnInfo(name = "album_id")
    val albumId: Long,
    @SerializedName("title")
    @ColumnInfo(name = "title")
    val title: String,
    @SerializedName("url")
    @ColumnInfo(name = "url")
    val url: String,
    @SerializedName("thumbnailUrl")
    @ColumnInfo(name = "thumbnail_url")
    val thumbnailUrl: String
) : Parcelable