package crazy.ones.sheypoorsample.feature

import crazy.ones.sheypoorsample.business.network.networkModule
import crazy.ones.sheypoorsample.business.persistence.databaseModule
import crazy.ones.sheypoorsample.feature.photos.photosModule
import org.koin.core.module.Module

val appModules: List<Module> = listOf(
    networkModule,
    databaseModule,
    photosModule
)