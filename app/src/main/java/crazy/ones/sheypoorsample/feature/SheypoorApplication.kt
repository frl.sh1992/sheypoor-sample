package crazy.ones.sheypoorsample.feature

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SheypoorApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@SheypoorApplication)
            modules(appModules)
        }
    }
}