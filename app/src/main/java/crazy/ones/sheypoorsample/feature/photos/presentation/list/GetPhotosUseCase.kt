package crazy.ones.sheypoorsample.feature.photos.presentation.list

import androidx.lifecycle.LiveData
import crazy.ones.sheypoorsample.business.persistence.PhotoDao
import crazy.ones.sheypoorsample.core.BaseUseCase
import crazy.ones.sheypoorsample.feature.photos.Photo
import crazy.ones.sheypoorsample.feature.photos.PhotoRepository

class GetPhotosUseCase(private val photoRepository: PhotoRepository, private val photoDao: PhotoDao): BaseUseCase<Unit, LiveData<List<Photo>>> {
    override suspend fun execute(input: Unit): LiveData<List<Photo>> {
        runCatching {
            photoRepository.getPhotos()
        }.onSuccess {
            photoDao.insertAll(it)
        }
        return photoDao.getAll()
    }
}