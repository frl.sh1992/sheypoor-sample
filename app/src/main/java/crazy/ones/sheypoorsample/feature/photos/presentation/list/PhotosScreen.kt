package crazy.ones.sheypoorsample.feature.photos.presentation.list

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.doOnPreDraw
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import crazy.ones.sheypoorsample.R
import crazy.ones.sheypoorsample.core.BaseFragment
import crazy.ones.sheypoorsample.core.LoadableData.*
import kotlinx.android.synthetic.main.screen_photos.*
import org.koin.android.ext.android.inject

class PhotosScreen : BaseFragment(R.layout.screen_photos) {
    private val viewModel: PhotosViewModel by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        photosList.adapter = PhotosAdapter { photo, sharedImage ->
            val extras = FragmentNavigatorExtras(
                sharedImage to photo.thumbnailUrl
            )
            findNavController().navigate(
                PhotosScreenDirections.actionOpenDetail(photo),
                extras
            )
        }
        viewModel.photosLiveData.observe(viewLifecycleOwner, Observer { loadableData ->
            emptyText.isGone = true
            toggleLoading(loadableData is Loading)
            when (loadableData) {
                Loading -> {
                }
                is Loaded -> {
                    loadableData.data.takeIf { it.isNotEmpty() }?.let { photos ->
                        (photosList.adapter as PhotosAdapter).submitList(photos)
                    } ?: run {
                        emptyText.isVisible = true
                    }
                }
                is Failed -> Toast.makeText(
                    context,
                    loadableData.errorMessage ?: "An error has been occurred",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
        postponeEnterTransition()
        photosList.doOnPreDraw { startPostponedEnterTransition() }
    }

    private fun toggleLoading(shouldShow: Boolean) {
        photosLoading.isVisible = shouldShow
        photosList.isVisible = !shouldShow
    }
}