package crazy.ones.sheypoorsample.feature.photos.presentation.detail

import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import androidx.annotation.RequiresApi
import androidx.navigation.fragment.navArgs
import crazy.ones.sheypoorsample.R
import crazy.ones.sheypoorsample.core.BaseFragment
import crazy.ones.sheypoorsample.core.load
import kotlinx.android.synthetic.main.screen_photo_detail.*

class PhotoDetailScreen : BaseFragment(R.layout.screen_photo_detail) {

    private val args: PhotoDetailScreenArgs by navArgs()

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData()
    }

    private fun setData() {
        args.photo.apply {
            photoDetailImage.apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    transitionName = thumbnailUrl
                }
                load(url)
            }
            photoDetailTitle.text = title
        }
    }
}