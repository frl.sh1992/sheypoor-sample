package crazy.ones.sheypoorsample.feature.photos

import retrofit2.http.GET

interface PhotosApi {
    @GET("photos")
    suspend fun getPhotos(): List<Photo>
}