package crazy.ones.sheypoorsample.feature.photos.presentation.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import crazy.ones.sheypoorsample.core.BaseViewModel
import crazy.ones.sheypoorsample.core.CoroutineDispatcherProvider
import crazy.ones.sheypoorsample.core.LoadableData
import crazy.ones.sheypoorsample.feature.photos.Photo
import crazy.ones.sheypoorsample.feature.photos.PhotoRepository
import kotlinx.coroutines.launch

class PhotosViewModel(coroutineDispatcherProvider: CoroutineDispatcherProvider,
    private val photoRepository: PhotoRepository
) : BaseViewModel(coroutineDispatcherProvider) {

    private val _photosLiveData: MutableLiveData<LoadableData<List<Photo>>> = MutableLiveData()
    val photosLiveData: LiveData<LoadableData<List<Photo>>>
        get() = _photosLiveData

    init {
        getPhotos()
    }

    fun getPhotos() {
        if (_photosLiveData.value is LoadableData.Loading)
            return
        _photosLiveData.value = LoadableData.Loading
        launch {
            onBg {
                runCatching {
                    photoRepository.getPhotos()
                }
            }.fold({ photos ->
                onUi {
                    _photosLiveData.value = LoadableData.Loaded(photos)
                }
            }, { error ->
                onUi{
                    _photosLiveData.value = LoadableData.Failed(error.message)
                }
                error.printStackTrace()
            })
        }
    }
}