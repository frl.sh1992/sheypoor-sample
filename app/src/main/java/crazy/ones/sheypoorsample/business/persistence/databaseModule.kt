package crazy.ones.sheypoorsample.business.persistence

import androidx.room.Room
import org.koin.dsl.module

internal val databaseModule = module {
    single {
        Room.databaseBuilder(
            get(),
            SheypoorDatabase::class.java, "sheypoor-database"
        ).build()
    }
    factory {
        get<SheypoorDatabase>().photoDao()
    }
}