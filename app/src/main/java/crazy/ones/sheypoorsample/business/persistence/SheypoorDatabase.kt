package crazy.ones.sheypoorsample.business.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import crazy.ones.sheypoorsample.feature.photos.Photo

@Database(entities = [Photo::class], version = 1)
abstract class SheypoorDatabase : RoomDatabase() {
    abstract fun photoDao(): PhotoDao
}