package crazy.ones.sheypoorsample.business.persistence

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import crazy.ones.sheypoorsample.feature.photos.Photo

@Dao
interface PhotoDao {
    @Query("SELECT * FROM photo")
    fun getAll(): LiveData<List<Photo>>

    @Query("SELECT * FROM photo WHERE id LIKE :id LIMIT 1")
    fun findById(id: Long): LiveData<Photo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(photo: List<Photo>)
}