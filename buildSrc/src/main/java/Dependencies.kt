object Versions {
    const val kotlin = "1.3.71"
    const val buildTools = "3.6.3"
    const val androidX = "1.3.0"
    const val appCompat = "1.1.0"
    const val constraintLayout = "1.1.3"
    const val junit = "4.13"
    const val coroutines = "1.3.0-RC"
    const val coroutinesAdapter = "0.9.2"
    const val retrofit = "2.9.0"
    const val gson = "2.8.6"
    const val koin = "2.1.5"
    const val coroutinesTest = "1.3.7"
    const val mockk = "1.10.0"
    const val lifecycle = "2.2.0"
    const val navigation = "2.3.0-beta01"
    const val okhttp = "3.12.8"
    const val glide = "4.11.0"
    const val picasso = "2.71828"
    const val arch = "2.1.0"
    const val room = "2.2.5"
}

object Plugins {
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val gradleBuildTools = "com.android.tools.build:gradle:${Versions.buildTools}"
    const val safeArgs = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
}

object Libs {
    const val kotlinStandard = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"

    // UI
    const val androidX = "androidx.core:core-ktx:${Versions.androidX}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val navigationUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    const val picasso = "com.squareup.picasso:picasso:${Versions.picasso}"

    // business
    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    const val coroutinesAdapter = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${Versions.coroutinesAdapter}"
    const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val gsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val koinScope = "org.koin:koin-androidx-scope:${Versions.koin}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    const val koinExperimental = "org.koin:koin-androidx-ext:${Versions.koin}"
    const val lifeCycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val lifeCycleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
    const val room = "androidx.room:room-runtime:${Versions.room}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
    const val roomKotlin = "androidx.room:room-ktx:${Versions.room}"

    // test
    const val junit = "junit:junit:${Versions.junit}"
    const val mockk = "io.mockk:mockk:${Versions.mockk}"
    const val koinTest = "org.koin:koin-test:${Versions.koin}"
    const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutinesTest}"
    const val archTest = "android.arch.core:core-testing:${Versions.arch}"
}